typedef struct list list;
typedef struct game game;
typedef struct unit unit;
typedef struct player player;
typedef struct map map;

int computeBucketIndex(int x, int y, map* map_data);
void update_unit(unit* unit_data, game* game_data, player* player);
void update_moving_unit(unit* unit_data, game* game_data);
void setup_player_unit_set(game* game, player* player_data);
int is_at_sight_distance(unit* unit_that_see, unit* other_unit);
void print_unit(unit* unit_data);
