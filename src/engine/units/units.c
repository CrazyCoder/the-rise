#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "units.h"
#include "../../domain/game.h"
#include "../../utils/linkedlist.h"


char* unit_type_str[] = { "Undef", "HQ", "GAS_EXTRACTOR", "GOLD_EXTRACTOR", "SOLDIER" };
int spawn_creation_time_by_unit[] = { 0, 10000000, 8000000, 30000000, 40000000 };
int sight_distance_by_unit[] = { 0, 50, 20, 20, 30 };

double get_distance(unit* unit1, unit* unit2);
void update_position(unit* unit_data, game* game_data);
unit* create_unit(int id, enum unit_type type, int health, double speed, int posx, int posy);
void add_unit_in_bucket(unit* unit_data, game* game_data, player* player_data);


void print_unit(unit* unit_data){
    printf("Unit [%s][H:%d][Pos:(%f,%f)][Speed:%f][Bucket:%d] \n", 
       unit_type_str[unit_data->type], 
       unit_data->health,
       unit_data->position.x,
       unit_data->position.y,
       unit_data->speed,
       unit_data->currentBucketIndex);
}

void update_unit_hq(unit* unit_data, game* game_data, player* player_data){
    list_item* next_move = unit_data->next_action_list->head;
    if(next_move != NULL){
        spawn_action* action = next_move->value;
        int elapsed_time = ((game_data->update_time.tv_sec - action->initial_time.tv_sec) * 1000000) + (game_data->update_time.tv_usec - action->initial_time.tv_usec);
        int unit_spawn_time = spawn_creation_time_by_unit[action->type_spawned];
        // printf("Update HQ: elapsed:%d, spawn:%d\n", elapsed_time, unit_spawn_time);
        if(elapsed_time >= unit_spawn_time){
            // printf("Update HQ. Creation time elapsed. Next move: %d\n", next_move->next);
            unit_data->next_action_list->head = next_move->next;

            unit* created_unit = create_unit(game_data->next_unit_id_generation++, action->type_spawned, 500, 0, 0, 0);
            addItem(player_data->units, created_unit);
            add_unit_in_bucket(created_unit, game_data, player_data);
            game_data->need_to_send_update = 1;
            free(action);
            free(next_move);
        }
    }
}

int is_at_sight_distance(unit* unit_that_see, unit* other_unit){
    double distance = get_distance(unit_that_see, other_unit);
    if(distance < unit_that_see->sight_distance){
        return 1;
    }

    return 0;
}

void update_unit(unit* unit_data, game* game_data, player* player_data){
    switch(unit_data->type){
        case HQ:
        {
            update_unit_hq(unit_data, game_data, player_data);
            return;
        }
        default:
            update_moving_unit(unit_data, game_data);
            return;
    }

    return;
}

void update_moving_unit(unit* unit_data, game* game_data){
    update_position(unit_data, game_data);
}

void add_unit_in_bucket(unit* unit_data, game* game_data, player* player_data){
    int x = (int)unit_data->position.x;
    int y = (int)unit_data->position.y;
    int bucketIndex = computeBucketIndex(x, y, game_data->map_data);
    unit_data->currentBucketIndex = bucketIndex;
    list* newBucktList = player_data->grid_buckets[bucketIndex].units;
    addItem(newBucktList, unit_data);
}

void setup_player_unit_set(game* game_data, player* player_data){

    // TODO it is hardcoded for new but should be more flexible by using conf
    int initial_pos_x = 100;
    int initial_pos_y = 100;
    if(player_data == &game_data->players[0]){
        initial_pos_x = 1000;
        initial_pos_y = 1000;
    }

    game_data->next_unit_id_generation = 1;
    unit* hq = create_unit(game_data->next_unit_id_generation++, HQ, 500, 0, initial_pos_x, initial_pos_y);
    addItem(player_data->units, hq);
    add_unit_in_bucket(hq, game_data, player_data);
    unit* unit = create_unit(game_data->next_unit_id_generation++, GAS_EXTRACTOR, 50, 1, initial_pos_x + 10, initial_pos_y + 10);
    addItem(player_data->units, unit);
    add_unit_in_bucket(unit, game_data, player_data);
    unit = create_unit(game_data->next_unit_id_generation++, GOLD_EXTRACTOR, 50, 1, initial_pos_x + 20, initial_pos_y + 10);
    addItem(player_data->units, unit);
    add_unit_in_bucket(unit, game_data, player_data);
}

unit* create_unit(int id, enum unit_type type, int health, double speed, int posx, int posy){
    unit* unit_to_create = calloc(1, sizeof(unit));
    unit_to_create->id = id;
    unit_to_create->type = type;
    unit_to_create->speed = speed;
    unit_to_create->health = health;
    unit_to_create->position.x = posx;
    unit_to_create->position.y = posy;
    unit_to_create->sight_distance = sight_distance_by_unit[type];
    unit_to_create->next_action_list = createList();
    return unit_to_create;
}

void update_position(unit* unit_data, game* game_data){
    list_item* next_move = unit_data->next_action_list->head;
    if(next_move != NULL){
        game_data->need_to_send_update = 1;

        // elapsed time is in nano
        double elapsed_time = (double)game_data->elapsed_time;
        vector* target = next_move->value;

        // speed of units is defined per sec
        vector move;
        move.x = target->x - unit_data->position.x;
        move.y = target->y - unit_data->position.y;
        double target_distance = sqrt((move.x *move.x) + (move.y * move.y));
        // printf("pos: (%f,%f), spped:%f\n", unit_data->position.x, unit_data->position.y, unit_data->speed);
        // printf("Target distance: %f, (%f,%f)\n", target_distance, move.x, move.y);
        move.x = (elapsed_time / 1000000.0) * unit_data->speed * move.x / target_distance;
        move.y = (elapsed_time / 1000000.0) * unit_data->speed * move.y / target_distance;
        double move_length = sqrt((move.x *move.x) + (move.y * move.y));
        // printf("Move: length:%f, (%f,%f)\n", move_length, move.x, move.y);
        if(move_length <= target_distance){
            
            unit_data->position.x = unit_data->position.x + move.x;
            unit_data->position.y = unit_data->position.y + move.y;
        }
        else {
            unit_data->position.x = target->x;
            unit_data->position.y = target->y;
            unit_data->next_action_list->head = next_move->next;
            free(target);
            free(next_move);
        }
        // printf("Move(%f,%f). length:%f\n", move.x, move.y, move_length);
        
        // printf("move from (%f,%f) to (%f,%f) in %f sec\n", unit_data->position.x, unit_data->position.y, move.x, move.y, elapsed_time/ 1000000.0);
    }
}

int computeBucketIndex(int x, int y, map* map_data){
    int ticks_x = x / map_data->bucketCount_x;
    int ticks_y = y / map_data->bucketCount_y;
    return ticks_x * MAP_VISIBILITY_RESOLUTION + ticks_y;
}
