typedef struct game game;
typedef struct observer observer;
typedef struct user_message user_message;
typedef struct player player;

game* create_new_game(int playerCount, char* map_name);

void start_game(game* game, observer* observer);

void on_user_message(user_message* msg, observer* observer);

void destroy_game(game* game_data);

void print_player(player* player_data);
