#include "../domain/game.h"
#include <math.h>


double get_distance(unit* unit1, unit* unit2){
    double part1 = unit1->position.x - unit2->position.x;
    double part2 = unit1->position.y - unit2->position.y;
    return sqrt(part1*part1 + part2*part2);
}
