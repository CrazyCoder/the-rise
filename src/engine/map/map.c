#include <stdio.h>
#include <stdlib.h>
#include "map.h"
#include "../../domain/game.h"

map* loadMap(char* name){
    map* map_data = calloc(1, sizeof(map));
    map_data->name = name;
    map_data->size_x = 1024;
    map_data->size_y = 1024;
    map_data->bucketCount_x = map_data->size_x / MAP_VISIBILITY_RESOLUTION;
    map_data->bucketCount_y = map_data->size_y / MAP_VISIBILITY_RESOLUTION;
    return map_data;
}
