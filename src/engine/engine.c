#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include<sys/time.h>
#include <unistd.h>
#include "engine.h"
#include "../domain/game.h"
#include "../domain/user.h"
#include "units/units.h"
#include "map/map.h"
#include "../utils/threadpool.h"
#include "../utils/linkedlist.h"

void update_game_on_tick(void* args);
void update_game_player_items(player* player, game* game_data);
void initializePlayer(game* g, player* player);

void print_player(player* player_data){
    printf("Player %s: [Gold:%d][Gas:%d]\n", player_data->id, player_data->gold, player_data->gas);
    printf("- Units :\n");
    list_item* item = player_data->units->head;
    while(item != NULL)
    {
        print_unit(item->value);
        item = item->next;
    }
}

void destroy_list_item(__attribute__ ((unused)) void* arg){
}

void destroy_list_item_generic(void* arg){
    free(arg);
}

void destroy_list_unit_item(void* arg){
    unit* unit_to_detroy = arg;
    destroy_list(unit_to_detroy->next_action_list, destroy_list_item_generic);
    free(unit_to_detroy);
}

int authenticate(user* usr, user_message* msg){
    if(msg->action == AUTH){
        usr->state = 1;
        return 1;
    }
    else {
        return 0;
    }
}

void spawn_unit(void* arg){
    // printf("Execute dispatched Spawn unit\n");
    spawn_action* action = arg;
    list_item* item = action->player->units->head;
    while(item != NULL)
    {
        unit* unit_data = item->value;
        if(unit_data->id == action->unit_id){
            action->initial_time = action->game->update_time;
            addItem(unit_data->next_action_list, action);
            return;
        }

        item = item->next;
    }

    free(action);
}

void move_unit(void* arg){
    move_action* action = arg;
    list_item* item = action->player->units->head;
    while(item != NULL)
    {
        unit* unit_data = item->value;
        if(unit_data->id == action->unit_id){
            addItem(unit_data->next_action_list, action->target);
            return;
        }

        item = item->next;
    }

    free(action);
}

void on_user_message(user_message* msg, observer* obs){
    switch(msg->action)
    {
        case START_QUICK_GAME: // start a new game
        {
            // TODO Try to find another player that is waiting to play
            // If after few seconds nobody are available start with a robot.


            game* game_data = create_new_game(2, "map_demo");
            game_data->need_to_send_update = 1;
            start_game(game_data, obs);
            printf("Created game %s : %s VS %s\n", game_data->name, game_data->players[0].id, game_data->players[1].id);
            break;
        }

        case NEW_PRIVATE_GAME:
        {
            break;
        }

        case SPAWN_UNIT:
        {
            spawn_action* action = calloc(1, sizeof(spawn_action));
            action->type = SPAWN_UNIT;
            action->unit_id = atoi(msg->payload1);
            action->type_spawned = strtoul(msg->payload2, NULL, 0);
            action->player = obs->player;
            action->game = obs->game_data;
            dispatch_seq(obs->game_data->threadpool_seq_id, spawn_unit, (void*)action);
            break;
        }

        case MOVE_UNIT:
        {
            move_action* action = calloc(1, sizeof(move_action));
            char *end_ptr;
            action->unit_id = strtol(msg->payload1, &end_ptr, 10);
            action->target = calloc(1, sizeof(vector));
            action->target->x = strtod(msg->payload2, &end_ptr);
            action->target->y = strtod(msg->payload3, &end_ptr);
            action->player = obs->player;
            action->game = obs->game_data;
            dispatch_seq(obs->game_data->threadpool_seq_id, move_unit, (void*)action);
            break;
        }
        default:
            break;
    }    
}

game* create_new_game(int playerCount, char* map_name){
    game* game_ptr = calloc(1, sizeof(game));
    game_ptr->name = "Game1";
    game_ptr->playerCount = playerCount;
    game_ptr->players[0].id = "P1";
    game_ptr->players[1].id = "P2";
    game_ptr->observers = createList();
    game_ptr->map_data = loadMap(map_name);
    game_ptr->threadpool_seq_id = get_sequencer_id();
    return game_ptr;
}

void start_game(game* game, observer* obs){
    game->startTime = time(NULL);
    initializePlayer(game, &(game->players[0]));
    initializePlayer(game, &(game->players[1]));
    addItem(game->observers, obs);
    obs->player = &game->players[0];
    obs->game_data = game;
    obs->data.visible_units = createList();
    struct timeval st;
    gettimeofday(&st,NULL);
    game->update_time = st;
    obs->type = INIT_GAME;
    obs->notify_user(obs);
    addPeriodicFunction(update_game_on_tick, game);
}

void initializePlayer(game* game_data, player* player_data){
    printf("initializePlayer.\n");
    player_data->gold = 1000;
    player_data->gas = 50;
    player_data->units = createList();
    for(int i = 0; i< MAP_VISIBILITY_RESOLUTION;i++){
        for(int j = 0; j< MAP_VISIBILITY_RESOLUTION;j++){
            int index = MAP_VISIBILITY_RESOLUTION * i + j;
            player_data->grid_buckets[index].units = createList();
        }
    }

    setup_player_unit_set(game_data, player_data);
}

void clean_player(player* player_data){
    printf("clean_player.\n");
    if(player_data->units != NULL){
        destroy_list(player_data->units, destroy_list_unit_item);
    }

    for(int i = 0; i< MAP_VISIBILITY_RESOLUTION * MAP_VISIBILITY_RESOLUTION;i++){
        destroy_list(player_data->grid_buckets[i].units, destroy_list_item);
    }
}

void destroy_game(game* game_data){
    if(game_data->playerCount > 0 ){ clean_player(&game_data->players[0]); }
    if(game_data->playerCount > 1 ){ clean_player(&game_data->players[1]); }
    if(game_data->playerCount > 2 ){ clean_player(&game_data->players[2]); }
    if(game_data->playerCount > 3 ){ clean_player(&game_data->players[3]); }
    destroy_list(game_data->observers, destroy_list_item_generic);
    free(game_data->map_data);
    free(game_data);
}

void set_observable_data(observer* obs, game* game_data){
    // Add all visible opponent units into the observable data
    player* player_data = obs->player;

    // we iterate on all units of the player
    list_item* item = player_data->units->head;
    while(item != NULL)
    {
        unit* unit_data = item->value;
        int bucketIndex = unit_data->currentBucketIndex;
        for(int i=0; i<game_data->playerCount;i++){
            player other_player = game_data->players[i];
            if(other_player.id ==obs->player->id){
                // If it is the current player we skip
                continue;
            }

            if(other_player.grid_buckets[bucketIndex].units->count == 0){
                continue;
            }

            list_item* other_player_item = other_player.grid_buckets[bucketIndex].units->head;
            while(other_player_item != NULL){
                if(is_at_sight_distance(unit_data, other_player_item->value)){
                    addItem(obs->data.visible_units, unit_data);
                }
                
                other_player_item = other_player_item->next;
            }
        }

        item = item->next;
    }
}

void update_game(game* game_data){
    // Update each player units
    update_game_player_items(&(game_data->players[0]), game_data);
    update_game_player_items(&(game_data->players[1]), game_data);

    if(game_data->need_to_send_update)
    {
        printf("Need to send update.\n");
        list_item* item = game_data->observers->head;
        while(item != NULL)
        {
            printf("Send update on observer.\n");
            observer* obs = item->value;
            obs->type = UPDATE;
            set_observable_data(obs, game_data);
            obs->notify_user(obs);
            item = item->next;
        }
    }
}

void update_game_on_tick(void* args){
    struct timeval st, et;
    gettimeofday(&st,NULL);
    // printf("Game update started.\n");
    
    game* game_data = (game*)args;
    game_data->last_update_time = game_data->update_time;
    game_data->update_time = st;
    game_data->elapsed_time = ((st.tv_sec - game_data->last_update_time.tv_sec) * 1000000) + (st.tv_usec - game_data->last_update_time.tv_usec);
    // printf("elapsed time: %d micro seconds\n",game_data->elapsed_time);
    gettimeofday(&et,NULL);
    int elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec);
    update_game(game_data);

    // game_data->need_to_send_update = 0;
    printf("propagate time: %d micro seconds\n",elapsed);
}

void update_unit_visibility(unit* unit_data, game* game_data, player* player_data){
    int x = (int)unit_data->position.x;
    int y = (int)unit_data->position.y;
    int newBucketIndex = computeBucketIndex(x, y, game_data->map_data);
    if (newBucketIndex != unit_data->currentBucketIndex){
        // If the unit has moved enough to change his bucket
        list* currentBucktList = player_data->grid_buckets[unit_data->currentBucketIndex].units;
        removeItem(currentBucktList, unit_data);
        unit_data->currentBucketIndex = newBucketIndex;
        list* newBucktList = player_data->grid_buckets[newBucketIndex].units;
        addItem(newBucktList, unit_data);
    }
}

void update_game_player_items(player* player, game* game_data){
    // printf("update items of player %s\n", player->id);
    list_item* item = player->units->head;
    while(item != NULL)
    {
        unit* unit_data = item->value;
        // printf("update item %d\n", unit_data->type);
        update_unit(unit_data, game_data, player);
        update_unit_visibility(unit_data, game_data, player);
        item = item->next;
    }
}
