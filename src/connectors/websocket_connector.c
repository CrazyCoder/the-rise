#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../engine/engine.h"
#include "../domain/user.h"
#include "../domain/game.h"
#include "../../lib/websocket/include/ws.h"
#include "../utils/linkedlist.h"

static list* user_observers;

void remove_user(int fd){
    if(user_observers->count == 0){
        return;
    }

    list_item* prev = NULL;
    list_item* item = user_observers->head;
    while(item != NULL){
        user* current_user = item->value;
        if(current_user->file_descriptor == fd)
        {
            if(prev == NULL){
                user_observers->head = item->next;
            }
            else{
                prev->next = item->next;
            }

            free(((observer*)(item->value))->user_ptr);
            free(item->value);
            free(item);
            return;
        }

        prev = item;
        item = item->next;
    }
}

char* buildUnitMessage(unit* unit_data, char* buffer){
    buffer[0] = 'U';
    buffer[1] = '|';
    buffer = buffer + 2;
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%d|", unit_data->id);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%d|", unit_data->type);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%d|", unit_data->health);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%f|", unit_data->speed);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%f|", unit_data->position.x);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%f|", unit_data->position.y);
    return buffer;
}

void buildUpdateMessage(observable_data data, char* buffer){
    buffer[0] = 'G';
    buffer[1] = '|';

    buffer = buffer + 2;
    list_item* item = data.visible_units->head;
    while(item != NULL){
        unit* unit_data = item->value;
        buffer = buildUnitMessage(unit_data, buffer);
        item = item->next;
    }

    buffer[0] = '=';
    buffer[1] = '|';
    buffer[2] = '\0';
}

void buildInitPlayerMessage(player* player_data, char* buffer){
    *(buffer++) = 'I';
    *(buffer++) = '|';
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%s|", player_data->id);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%d|", player_data->gold);
    buffer += snprintf(buffer, MESSAGE_BUFFER_SIZE, "%d|", player_data->gas);
    list_item* item = player_data->units->head;
    while(item != NULL){
        unit* unit_data = item->value;
        buffer = buildUnitMessage(unit_data, buffer);
        item = item->next;
    }
    buffer[0] = '=';
    buffer[1] = '|';
    buffer[2] = '\0';
}

void on_notification(observer* obs){
    printf("Notify the user\n");
    if(obs->type == UPDATE)
    {
        buildUpdateMessage(obs->data, obs->message_buffer);
    }
    else if(obs->type == INIT_GAME)
    {
        buildInitPlayerMessage(obs->player, obs->message_buffer);
    }
    
    printf("msg sent : %s", obs->message_buffer);
    ws_sendframe_txt(obs->user_ptr->file_descriptor, obs->message_buffer, false);
}

observer* find_observer(int fd){
    printf("find observer.\n");
    if(user_observers->count == 0){
        return NULL;
    }

    list_item* item = user_observers->head;
    while(item != NULL){
        observer* current_observer = item->value;
        if(current_observer->user_ptr->file_descriptor == fd)
        {
            printf("end observer.\n");
            return current_observer;
        }

        item = item->next;
    }

    printf("end null observer.\n");
    return NULL;
}

/**
 * @brief This function is called whenever a new connection is opened.
 * @param fd The new client file descriptor.
 */
void onopen(int fd)
{
    char *cli;
    cli = ws_getaddress(fd);
    user* new_user = calloc(1, sizeof(user));
    new_user->file_descriptor = fd;
    observer* obs = calloc(1, sizeof(observer));
    obs->id = 142;
    obs->user_ptr = new_user;
    obs->notify_user = on_notification;
    addItem(user_observers, obs);
    printf("Connection opened, client: %d | addr: %s\n", fd, cli);
    free(cli);
}

/**
 * @brief This function is called whenever a connection is closed.
 * @param fd The client file descriptor.
 */
void onclose(int fd)
{
    char *cli;
    cli = ws_getaddress(fd);
    printf("Connection closed, client: %d | addr: %s\n", fd, cli);
    remove_user(fd);
    free(cli);
}

user_message* parseMessage(const char *incoming_msg, uint64_t size){
    user_message* msg = calloc(1, sizeof(user_message));
    char msg_word[16];
    int wordCount = 0;
    size_t wordIndex = 0;
    for(uint64_t i=0; i< size;i++){
        
        if(incoming_msg[i] == '|'){
            wordCount++;
            msg_word[wordIndex] = '\0';
            if(wordCount == 1)
            {
                msg->action = (enum action_type)atoi(msg_word);
            }
            else if(wordCount == 2)
            {
                char* payload = malloc(wordIndex * sizeof(char));
                strncpy(payload, msg_word, wordIndex);
                msg->payload1 = payload;
            }
            else if(wordCount == 3)
            {
                char* payload = malloc(wordIndex * sizeof(char));
                strncpy(payload, msg_word, wordIndex);
                msg->payload2 = payload;
            }
            else if(wordCount == 4)
            {
                char* payload = malloc(wordIndex * sizeof(char));
                strncpy(payload, msg_word, wordIndex);
                msg->payload3 = payload;
            }

            wordIndex = 0;
        }
        else {
            msg_word[wordIndex++] = incoming_msg[i];
        }
        
    }

    printf("msg : %d, %s, %s, %s\n", msg->action, msg->payload1, msg->payload2, msg->payload3);
    return msg;
}

void destroy_message(user_message* msg){
    if(msg->payload1 != NULL){ free(msg->payload1); }
    if(msg->payload2 != NULL){ free(msg->payload2); }
    if(msg->payload3 != NULL){ free(msg->payload3); }
    free(msg);
}

/**
 * @brief Message events goes here.
 * @param fd   Client file descriptor.
 * @param msg  Message content.
 * @param size Message size.
 * @param type Message type.
 */
void onmessage(int fd, const unsigned char *msg, uint64_t size, int type)
{
    char *cli;
    cli = ws_getaddress(fd);
    printf("I receive a message[type=%d]: %s (%llu), from: %s/%d\n", type, msg, size, cli, fd);
    user_message* user_msg = parseMessage((char*)msg, size);
    observer* current_observer = find_observer(fd);
    if(current_observer == NULL){
        printf("Cannot find the user in the cache.\n");
    }
    else {
        printf("User found in the cache.\n");
    }

    if(user_msg->action == 1){
        printf("call authnetication.\n");
        if(authenticate(current_observer->user_ptr, user_msg)){
            ws_sendframe_txt(fd, "1|OK|=|", false);
        }
        else {
            ws_sendframe_txt(fd, "1|KO|bad credentials|=|", false);
        }
        
    }
    else {
        on_user_message(user_msg, current_observer);
    }
    
    destroy_message(user_msg);
    free(cli);
}

void start_server()
{
    user_observers = createList();
    struct ws_events evs;
    evs.onopen    = &onopen;
    evs.onclose   = &onclose;
    evs.onmessage = &onmessage;

    ws_socket(&evs, 8080, 1);
}
