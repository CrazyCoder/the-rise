typedef struct player player;
typedef struct game game;
typedef struct list list;

typedef struct user {
    int file_descriptor;
    int state; // User possible states : 0 : not connected, 1: connected
} user;

#define MESSAGE_BUFFER_SIZE 4096

enum action_type { 
    AUTH = 1, START_QUICK_GAME = 2, NEW_PRIVATE_GAME = 3, JOIN_PRIVATE_GAME = 4, 
    SPAWN_UNIT = 5, MOVE_UNIT = 6,
};

enum message_notification_type {
    INIT_GAME = 1, UPDATE=2
};

typedef struct user_message {
    enum action_type action;
    char* payload1;
    char* payload2;
    char* payload3;
} user_message;

typedef struct observable_data {
    list* visible_units;
} observable_data;

typedef struct observer {
    int id;
    user* user_ptr;
    player* player;
    enum message_notification_type type;
    game* game_data;
    observable_data data;
    char message_buffer[MESSAGE_BUFFER_SIZE];
    void (*notify_user)(struct observer*);
} observer;

int authenticate(user* usr, user_message* msg);
