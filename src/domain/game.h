#include <time.h>
#include<sys/time.h>
#include "../utils/vector.h"

#define MAP_VISIBILITY_RESOLUTION 16

typedef struct list list;

enum unit_type { HQ=1, GAS_EXTRACTOR=2, GOLD_EXTRACTOR=3, SOLDIER=4 };

typedef struct element {
    int type;
    vector position;
} element;

typedef struct grid_bucket {
    list* units;
} grid_bucket;

typedef struct player  {
    char* id;
    int gold;
    int gas;
    list* units;
    grid_bucket grid_buckets[MAP_VISIBILITY_RESOLUTION * MAP_VISIBILITY_RESOLUTION]; // It is a matrix of list of units
} player;

typedef struct unit {
    enum unit_type type;
    int id;
    int health;
    int sight_distance;
    vector position;
    double speed;
    list* next_action_list;
    int currentBucketIndex;

} unit;

typedef struct map{
    int id;
    char* name;
    int size_x;
    int size_y;
    int bucketCount_x;
    int bucketCount_y;
    int heightfield[1024][1024];
    list* elements;

} map;

typedef struct game {
    char*   name;
    time_t  startTime;
    player  players[4];
    int     playerCount;
    list*   observers;
    map*    map_data;
    struct timeval  update_time;
    struct timeval  last_update_time;
    int elapsed_time;
    int need_to_send_update;
    int next_unit_id_generation;
    int threadpool_seq_id;

} game;

typedef struct spawn_action {
    int type;
    int unit_id;
    enum unit_type type_spawned;
    struct timeval initial_time;
    player* player;
    game* game;

} spawn_action;

typedef struct move_action {
    int type;
    int unit_id;
    vector* target;
    player* player;
    game* game;

} move_action;

