typedef void (*task_function)(void *arg);

// Initialize the thread pool
void initializePool(int threadCount);

// Dispatch a task to be executed
void dispatch(task_function function, void* args);
void dispatch_seq(int sequencer_id, task_function function, void* args);
void join();
void disposePool();
int get_sequencer_id();

// Add a function that will be triggered every game engine tick
void addPeriodicFunction(task_function function, void* args);