#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "threadpool.h"
#include "linkedlist.h"

void* run(void *arg);
void* runPeriodic(void *arg);
typedef struct pool {
    int id;
    int threadCount;
    pthread_t periodicThread;
    pthread_t tid[4];
    list* task_list;
    list* periodic_task_list;
    int state;
} pool;

typedef struct task {
    int id;
    task_function functionToExecute;
    void* args;
} task;

typedef struct sequencer_block {
    list* task_queue;
    pthread_mutex_t* mutex;
    int is_running;
} sequencer_block;


static pool threadpool;
static sequencer_block* sequencers;
static list* available_sequencers;

void initializePool(int threadCount){
    printf("Threadpool init started...\n");
    printf("Threadpool threads created...\n");
    threadpool.threadCount = threadCount;
    threadpool.task_list = createList();
    threadpool.periodic_task_list = createList();
    threadpool.state = 1;
    available_sequencers = createList();
    sequencers = calloc(128, sizeof(sequencer_block));
    for(int i=0; i<128; i++){
        sequencers[i].mutex = calloc(1, sizeof(pthread_mutex_t));
        pthread_mutex_init(sequencers[i].mutex, NULL);
        sequencers[i].task_queue = createList();
        addItem(available_sequencers, (void*)i);
    }

    pthread_create(&(threadpool.periodicThread), NULL, &runPeriodic, NULL);
    for(int i=0; i< threadCount; i++){
        printf("Threadpool create thread %d...\n", i);
        int localId = i;
        pthread_create(&(threadpool.tid[i]), NULL, &run, (void*)localId);
    }
}

// Dispatch a task to be executed
void dispatch(task_function function, void* args){
    task* task_ptr = calloc(1, sizeof(task));
    task_ptr->args = args;
    task_ptr->functionToExecute = function;
    addItem(threadpool.task_list, task_ptr);
}

void execute_sequenced_task(void* arg){
    printf("execute_sequenced_task started \n");
    sequencer_block* block = arg;

    task* task_to_execute = NULL;
    pthread_mutex_lock(block->mutex);
        block->is_running = 1;
        task_to_execute = popItem(block->task_queue);
    pthread_mutex_unlock(block->mutex);

    if(task_to_execute != NULL){
        task_to_execute->functionToExecute(task_to_execute->args);
    }

    pthread_mutex_lock(block->mutex);
        block->is_running = 0;
        if(block->task_queue->count > 0){
            printf("request exec of next task. \n");
            task* task_ptr = calloc(1, sizeof(task));
            task_ptr->args = block;
            task_ptr->functionToExecute = execute_sequenced_task;
            addItem(threadpool.task_list, task_ptr);
        }
    pthread_mutex_unlock(block->mutex);
    printf("execute_sequenced_task ended \n");
}

void dispatch_seq(int sequencer_id, task_function function, void* args){
    printf("dispatch_seq started \n");
    task* sequenced_task_ptr = calloc(1, sizeof(task));
    sequenced_task_ptr->args = args;
    sequenced_task_ptr->functionToExecute = function;
    
    pthread_mutex_lock(sequencers[sequencer_id].mutex);
        addItem(sequencers[sequencer_id].task_queue, sequenced_task_ptr);
        if(!sequencers[sequencer_id].is_running){
            printf("dispatch_seq no running task \n");
            sequencers[sequencer_id].is_running = 1;
            task* task_ptr = calloc(1, sizeof(task));
            task_ptr->args = &sequencers[sequencer_id];
            task_ptr->functionToExecute = execute_sequenced_task;
            addItem(threadpool.task_list, task_ptr);
        }
    pthread_mutex_unlock(sequencers[sequencer_id].mutex);
}

void join(){
    pthread_join(threadpool.tid[0], NULL);
    pthread_join(threadpool.tid[1], NULL);
}

int get_sequencer_id(){
    if(available_sequencers->count == 0){
        printf("No more available sequencers.\n");
        return -1;
    }

    return (int) popItem(available_sequencers);
}

void disposePool(){
    threadpool.state = 0;
}

void addPeriodicFunction(task_function function, void* args){
    task* task_ptr = calloc(1, sizeof(task));
    task_ptr->args = args;
    task_ptr->functionToExecute = function;
    addItem(threadpool.periodic_task_list, task_ptr);
}

void* run(void *arg){
    int threadId = (int)arg;

    while(threadpool.state)
    {
        task* taskToExecute = (task*)popItem(threadpool.task_list);
        if(taskToExecute != NULL){
            // printf("Threadpool[%d] execute task...\n", threadId);
            taskToExecute->functionToExecute(taskToExecute->args);
            free(taskToExecute);
        }
        else {
            usleep(10);
        }
    }

    printf("Threadpool[%d] shutdown...\n", threadId);
    return 0;
}

void* runPeriodic(void *arg){
    if(arg != 0)
    {
        return 0;
    }

    printf("Periodic thread started...\n");
    while(threadpool.state)
    {
        list_item* item = threadpool.periodic_task_list->head;
        while(item != NULL)
        {
            // printf("Periodic thread add task to execute...\n");
            task* taskToExecute = (task*)item->value;
            dispatch(taskToExecute->functionToExecute, taskToExecute->args);
            item = item->next;
        }

        usleep(1000000);
    }

    printf("Periodic thread terminated...\n");
    return 0;
}