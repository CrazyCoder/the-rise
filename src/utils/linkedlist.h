#include <pthread.h>

typedef void (*content_destroyer_function)(void *arg);

typedef struct list_item {
    int id;
    void* value;
    struct list_item* next;
} list_item;

typedef struct list {
    list_item* head;
    list_item* tail;
    int count;
    pthread_mutex_t* mutex;
} list;

list* createList();

void destroy_list(list* list, content_destroyer_function function);

void addItem(list* list, void* value);
void removeItem(list* list, void* value);
void* popItem(list* list);

list_item* getListItemAt(list* list_ptr, int index);
