#include <stdlib.h>
#include <stdio.h>
#include "linkedlist.h"


list* createList(){
    list* list_ptr = calloc(1, sizeof(list));
    list_ptr->mutex = calloc(1, sizeof(pthread_mutex_t));
    pthread_mutex_init(list_ptr->mutex, NULL);
    return list_ptr;
}

void destroy_list(list* list_ptr, content_destroyer_function function){
    list_item* item = list_ptr->head;
    while(item != NULL){
        list_item* itemTemp = item;
        item = item->next;
        function(itemTemp->value);
        free(itemTemp);
    }

    list_ptr->head = NULL;
    pthread_mutex_destroy(list_ptr->mutex);
    free(list_ptr->mutex);
    free(list_ptr);
}

static int identifier = 0;
void addItem(list* list, void* value){
    list_item* item = calloc(1, sizeof(list_item));
    item->value = value;

    pthread_mutex_lock(list->mutex);
    item->id = identifier++;
    if(list->tail != NULL){
        list->tail->next = item;
    }
    if(list->head == NULL){
        list->head = item;
    }
    
    item->next = NULL;
    list->tail = item;
    list->count = list->count+1;
    // printf("addItem %d. Next:%d\n", item->id, item->next);
    pthread_mutex_unlock(list->mutex);
}

void removeItem(list* list, void* value){
    list_item* item = list->head;
    if(item->value == value){
         list->head = item->next;
         free(item);
         return;
    }

    list_item* previous_item = item;
    item = item->next;
    while(item != NULL){
        if(item->value == value){
            previous_item->next = item->next;
            free(item);
            return;
        }

        previous_item = item;
        item = item->next;
    }
}

void* popItem(list* list_ptr){
    pthread_mutex_lock(list_ptr->mutex);
    // printf("popItem enter lock\n");
    list_item* item = list_ptr->head;
    if(item == NULL){
        pthread_mutex_unlock(list_ptr->mutex);
        return NULL;
    }
    
    list_ptr->head = item->next;
    void* value = item->value;
    list_ptr->count = list_ptr->count-1;
    // printf("popItem poped %d. Remaining items: %d. Next item:%d\n", item->id, list_ptr->count, list_ptr->head);
    // printf("popItem exit lock\n");
    pthread_mutex_unlock(list_ptr->mutex);
    free(item);
    return value;
}

list_item* getListItemAt(list* list_ptr, int index)
{
    return NULL;
}
