#include <stdio.h>
#include "domain/game.h"
#include "engine/engine.h"
#include "utils/threadpool.h"
#include "connectors/connector.h"

int main()
{

    initializePool(2);
    start_server();
    join();
    return 0;
}
