#include <stdio.h>
#include <unistd.h>
#include "../utils/threadpool.h"

typedef struct test_arg {
    int task_id;
    int is_running;
    struct test_arg* other_thread_arg;
    int should_dispose;
} test_arg;

void long_parallel_time_function(void* arg){
    test_arg* task = arg;
    task->is_running = 1;
    usleep(1000000);
    if(task->other_thread_arg->is_running == 1){
        disposePool();
    }
}

void long_sequenced_time_function(void* arg){
    printf("long_sequenced_time_function started \n");
    test_arg* task = arg;
    task->is_running = 1;
    usleep(2000000);
    if(task->other_thread_arg->is_running == 1){
        printf("Test failed. Another task is running in parallel.\n");
        disposePool();
    }

    task->is_running = 0;
    if(task->should_dispose){
        disposePool();
    }
}

void check_that_games_tasks_are_sequenced(){
    printf("Start unit test threadpool sequence\n");
    initializePool(2);
    usleep(500);
    test_arg argument1= { .task_id = 0, .is_running = 0, .should_dispose = 0 };
    test_arg argument2= { .task_id = 1, .is_running = 0, .should_dispose = 1 };
    argument1.other_thread_arg = &argument2;
    argument2.other_thread_arg = &argument1;
    dispatch_seq(1, long_sequenced_time_function, (void*) &argument1);
    dispatch_seq(1, long_sequenced_time_function, (void*) &argument2);
    join();
    printf("End unit test threadpool sequence\n");
}

void check_that_2_different_games_tasks_are_parallel(){
    printf("Start unit test threadpool parallel\n");
    initializePool(2);
    usleep(500);
    test_arg argument1= { .task_id = 0, .is_running = 0 };
    test_arg argument2= { .task_id = 1, .is_running = 0 };
    argument1.other_thread_arg = &argument2;
    argument2.other_thread_arg = &argument1;
    dispatch_seq(1, long_parallel_time_function, (void*) &argument1);
    dispatch_seq(2, long_parallel_time_function, (void*) &argument2);
    join();
    printf("End unit test threadpool parallel\n");
}

int main()
{
    check_that_2_different_games_tasks_are_parallel();
    check_that_games_tasks_are_sequenced();
    return 0;
}