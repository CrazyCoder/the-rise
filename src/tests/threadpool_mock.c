#include <stddef.h>
#include "../utils/threadpool.h"

void dispatch(task_function function, void* args){
    function(args);
}

void dispatch_seq(int sequencer_id, task_function function, void* args){
    function(args);
}

int get_sequencer_id(){
    return 1;
}

void addPeriodicFunction(task_function function, void* args){
    if(function == NULL || args == NULL){
        return;
    }
}