#include <stdio.h>
#include <stddef.h>
#include <math.h>
#define EPSILON 0.000001

int assert_not_null(void* ptr){
    return ptr != NULL;
}

void assert_int_equals(int a, int b, char* error_message){
    if(a != b){
        printf(error_message, a, b);
    }
}

int assert_double_equals(double a, double b){
    if(fabs(a-b) < EPSILON){
        return 1;
    }

    return 0;
}