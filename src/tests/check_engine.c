#include <stdio.h>
#include <stdlib.h>
#include "../domain/game.h"
#include "../domain/user.h"
#include "../utils/linkedlist.h"

void on_user_message(user_message* msg, observer* observer);
int assert_double_equals(double a, double b);
int assert_not_null(void* ptr);
void assert_int_equals(int a, int b, char* error_message);
void update_game(game* g);
void start_game(game* g, observer* obs);
game* create_new_game(int i, char* name);
void destroy_game(game* game_data);
void print_player(player* player_data);
void print_unit(unit* unit_data);

void assertItem(unit* unit_data, vector expectedPosition, double expectedSpeed){
    if(assert_double_equals(unit_data->position.x, expectedPosition.x) && 
        assert_double_equals(unit_data->position.y, expectedPosition.y) && 
        assert_double_equals(expectedSpeed, unit_data->speed)){
    }
    else {
        printf("** Assert Unit failed :\n");
        print_unit(unit_data);
        printf("Assertion failed : unit(%f, %f) != expected(%f, %f), speed(%f) != %f\n", unit_data->position.x, unit_data->position.y, expectedPosition.x, expectedPosition.y, unit_data->speed, expectedSpeed);
    }
}

void on_notification(observer* obs){
    printf("Notify the user %d\n", obs->id);
}

game* createGame(){
    game* g = create_new_game(2, "map1");
    return g;
}

void test_spawn_gas_collector(){
    printf("test_spawn_gas_collector Started.\n");
    game* game_data = createGame();
    observer* obs = calloc(1, sizeof(observer));
    obs->id = 142;
    obs->notify_user = on_notification;
    start_game(game_data, obs);
    struct timeval current_time;
    gettimeofday(&current_time,NULL);
    game_data->update_time = current_time;

    user_message* msg = calloc(1, sizeof(user_message));
    msg->action = SPAWN_UNIT;
    msg->payload1 = "1";
    msg->payload2 = "2";
    on_user_message(msg, obs);

    unit* hq = game_data->players[0].units->head->value;

    if(!assert_not_null(hq->next_action_list->head)){
        printf("Error HQ has no pending spawn item");
    }

    // We check that when a update is triggered before the expiration of the unit time creation there is no unit created
    game_data->elapsed_time = 6000000;
    game_data->update_time.tv_sec += 6;
    update_game(game_data);
    unit* moving_unit = game_data->players[0].units->head->next->value;
    if(moving_unit->id != 2 || moving_unit->type != 2){
        printf("Error the first moving unit is not expected\n");
    }

    moving_unit = game_data->players[0].units->head->next->next->value;
    if(moving_unit->id != 3 || moving_unit->type != 3){
        printf("Error the second moving unit is not expected\n");
    }

    if(game_data->players[0].units->head->next->next->next != NULL){
        printf("Error the third moving unit is not expected\n");
    }

    game_data->elapsed_time = 2500000;
    game_data->update_time.tv_sec += 2;
    update_game(game_data);

    if(game_data->players[0].units->head->next->next->next == NULL){
        printf("Error the third moving unit is not expected\n");
    }

    unit* created_unit = game_data->players[0].units->head->next->next->next->value;
    if(created_unit->type != GAS_EXTRACTOR || created_unit->id != 4 || created_unit->health != 500 || created_unit->speed != 0){
        printf("Error, the created unit is not the expected gas extractor. Created: {type: %d, id:%d, health:%d, speed: %f\n", created_unit->type, created_unit->id, created_unit->health, created_unit->speed);
    }

    if(game_data->players[0].units->head->next->next->next->next != NULL){
        printf("Error the third moving unit is not expected.\n");
    }

    // Check that once the unit is created, at the next update it is not created a second time
    game_data->elapsed_time = 2500000;
    game_data->update_time.tv_sec += 2;
    update_game(game_data);

    created_unit = game_data->players[0].units->head->next->next->next->value;
    if(created_unit->type != GAS_EXTRACTOR || created_unit->id != 4 || created_unit->health != 500 || created_unit->speed != 0){
        printf("Error, the created unit is not the expected gas extractor. Created: {type: %d, id:%d, health:%d, speed: %f\n", created_unit->type, created_unit->id, created_unit->health, created_unit->speed);
    }

    if(game_data->players[0].units->head->next->next->next->next != NULL){
        printf("Error the third moving unit is not expected.\n");
    }

    free(msg);
    destroy_game(game_data);
    printf("test_spawn_gas_collector Ended.\n");
}

void test_move_command(){

}

void test_moving_units()
{
    printf("test_moving_units Started.\n");
    game* game_data = createGame();
    observer* obs = calloc(1, sizeof(observer));
    obs->id = 142;
    obs->notify_user = on_notification;
    start_game(game_data, obs);
    game_data->elapsed_time = 1000000;

    user_message* msg = calloc(1, sizeof(user_message));
    msg->action = MOVE_UNIT;
    msg->payload1 = "2";
    msg->payload2 = "1005";
    msg->payload3 = "1004";
    on_user_message(msg, obs);

    vector expectedPositionP1 = { .x= 1000, .y=1000 };
    vector expectedPositionP2 = { .x= 100, .y=100 };
    vector expectedMovingPositionP1 = { .x= 1010, .y=1010 };
    vector expectedMovingPositionP2 = { .x= 110, .y=110 };
    assertItem(game_data->players[0].units->head->value, expectedPositionP1, 0);
    assertItem(game_data->players[0].units->head->next->value, expectedMovingPositionP1, 1);
    assertItem(game_data->players[1].units->head->value, expectedPositionP2, 0);
    assertItem(game_data->players[1].units->head->next->value, expectedMovingPositionP2, 1);

    update_game(game_data);

    vector expectedAfterUpdateMovingPositionP1 = { .x= 1009.359816, .y= 1009.231779 };
    assertItem(game_data->players[0].units->head->value, expectedPositionP1, 0);
    assertItem(game_data->players[0].units->head->next->value, expectedAfterUpdateMovingPositionP1, 1);
    assertItem(game_data->players[1].units->head->value, expectedPositionP2, 0);
    assertItem(game_data->players[1].units->head->next->value, expectedMovingPositionP2, 1);

    update_game(game_data);

    vector expectedAfter2UpdateMovingPositionP1 = { .x= 1008.719631, .y= 1008.463557 };
    assertItem(game_data->players[0].units->head->value, expectedPositionP1, 0);
    assertItem(game_data->players[0].units->head->next->value, expectedAfter2UpdateMovingPositionP1, 1);
    assertItem(game_data->players[1].units->head->value, expectedPositionP2, 0);
    assertItem(game_data->players[1].units->head->next->value, expectedMovingPositionP2, 1);

    game_data->elapsed_time = 6000000;
    update_game(game_data);

    vector expectedAfter3UpdateMovingPosition = { .x= 1005, .y= 1004 };
    assertItem(game_data->players[0].units->head->value, expectedPositionP1, 0);
    assertItem(game_data->players[0].units->head->next->value, expectedAfter3UpdateMovingPosition, 1);
    assertItem(game_data->players[1].units->head->value, expectedPositionP2, 0);
    assertItem(game_data->players[1].units->head->next->value, expectedMovingPositionP2, 1);

    destroy_game(game_data);
    printf("test_moving_units ended.\n");
}

void test_player_see_only_enemy_units_at_sight()
{
    printf("test_player_see_only_enemy_units_at_sight Started.\n");
    game* game_data = createGame();
    observer* obs = calloc(1, sizeof(observer));
    obs->id = 142;
    obs->notify_user = on_notification;
    start_game(game_data, obs);

    // Set the moving unit of player 1 in the sight of player 2
    vector* targetPosition = calloc(1, sizeof(vector));
    targetPosition->x = 1005;
    targetPosition->y = 1004;
    unit* moving_unit = (unit*)game_data->players[0].units->head->next->value;
    addItem(moving_unit->next_action_list, targetPosition);
    moving_unit->position.x = 102;
    moving_unit->position.y = 103;

    game_data->elapsed_time = 1000000;
    update_game(game_data);

    print_player(&game_data->players[0]);
    print_player(&game_data->players[1]);

    assert_int_equals(obs->data.visible_units->count, 3, "Visible units count failed. Observed:%d, expected:%d\n");
    


    destroy_game(game_data);
    printf("test_player_see_only_enemy_units_at_sight ended.\n");
}

int main(){
    // test_player_see_only_enemy_units_at_sight();
    //test_spawn_gas_collector();
    test_moving_units();
    //test_move_command();

    return 0;
}
