# Project name : The-rise



## Why this project?

The purpose of this game is to be a Starcraft like. I started it for educational purpose. To learn making a game in full C using the minimum of library. I named the project the-rise, rise of programming knowledge. It is not the game name.



## Description
It is greatly inspired from Starcraft 2. it is a 1v1 game where the player can grow and expand his colony in order to beat the opponent colony.
This project is only about the backend, not the UI. It means that it can be played by a UI client or by a bot.
The idea is to have an open communication protocol when anybody can build his own UI design.

### Available units

Command center (type=1, can spawn units)<br>
Gas extractor (type=2)<br>
Gold extractor (type=3)<br>
Soldier (type=4)<br>

## API

Message general format : MESSAGE_TYPE|WORD1|WORD2|...|

### Connect by websocket and send authentication message

Authentication message format : 1|username|password|<br>
Authentication response format : <br>
1|OK| if authentication is ok|<br>
1|KO|Reason| if authentication failed|<br>


### Start a quick game
message format : **2|PLAYER_COUNT|**

### Create private game 
message format : **3|PLAYER_COUNT|MAP_ID|GAME_NAME|**<br>
response : **3|OK|GAME_ID|**<br>

### Join private game 
message format : **4|GAME_ID|**

### IN-GAME messages

#### Game initialization message

Message format : **I|PLAYER_ID|GOLD_AMOUNT|GAS_AMOUNT|UNIT_1|UNIT_2|...|**

UNIT message format : **ID|UNIT_TYPE|HEALTH|SPEED**


#### Game updates sent to players

Update message format : **G|UPDATE1|UPDATE2|...|**
Unit UPDATEX format : **U|UNIT_ID|HEALTH|POS_X|POS_Y|TARGET_X|TARGET_Y|**

#### Commands player can send
Spawn a unit : **5|CREATOR_UNIT_ID|UNIT_TYPE|**<br>
Move a unit : **6|UNIT_ID|TARGET_X|TARGET_Y|**<br>

## Support
If you have ideas, suggestions you can create issues or merge request. I will be happy to integrate it if it is awesome.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
This project is open to contribution. It would be a pleasure to see people joining the projectand make it more active.

## License
TODO


